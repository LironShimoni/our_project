/* version 1.0 of my school*/

#include <iostream>
#include <string>
#include "C:\Users\magshimim\source\repos\git with liron\git with liron\Student.cpp"
#include "C:\Users\magshimim\source\repos\git with liron\git with liron\Course.cpp"

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;



Student* newStudent()
{
	string name;
	int crsCount;

	std::cout << "Enter student's name: ";
	cin >> name;
	std::cout << "Enter number of courses for student: ";
	cin >> crsCount;

	Course* crs = NULL;

	// handle of courses list
	Course** courses = new Course*[crsCount];
	for (int i = 0; i < crsCount; i++)
	{
		//Create new course
		string crsName;
		int test1, test2, exam;
		std::cout << "Enter Course name: ";
		cin >> crsName;
		std::cout << "Enter first test grade: ";
		cin >> test1;
		std::cout << "Enter second test grade: ";
		cin >> test2;
		cout << "Enter exam grade: ";
		cin >> exam;
		cout << endl;

		// create course object
		// TO DO: should create new Course object and assign it to crs 
		crs = new Course();

		// call to init function of course object
		crs->init(crsName, test1, test2, exam);

		//Add course to list
		courses[i] = crs;
	}


	Student* student;

	// create student object
	// TO DO: create new student object
	student = new Student();

	// call to init function of student object
	// TO DO: call the init function of students object
	student->init(name, courses, crsCount);


	// return student object
	return student;

}

int showMenu()
{
	int option = 0;
	std::cout << "Welcome to Magshimim! You have the following options:" << endl;
	std::cout << "1. Enter student deatails and courses" << endl;
	std::cout << "2. Calculate average grade of all courses" << endl;
	std::cout << "3. Show details of courses" << endl;
	std::cout << "4. Exit" << endl;
	std::cout << "Enter your choice: ";
	cin >> option;
	std::cout << endl;

	return option;

}

void printCourses(Course** list, int count)
{
	int i;
	Course* currCourse;
	int* gradesList;



	for (i = 0; i < count; ++i)
	{
		currCourse = list[i];

		// TO DO: shold get the course grades list into gradesList
		gradesList = currCourse->getGradesList();

		std::cout << "Name: " << currCourse->getName() << ", test1 = " << gradesList[0] << ", test2 = " << gradesList[1] << ", exam = " << gradesList[2] << ", Grade = " << currCourse->getFinalGrade() << endl;
	}
	cout << endl << endl;
}

int main()
{
	Student* student = NULL;
	double avg = 0;

	//Main Loop
	bool toExit = false;
	while (!toExit)
	{
		switch (showMenu())
		{
		case MENU_STUDENT_DETAILS:
			if (student)
			{
				delete student;
			}

			student = newStudent();

			break;
		case MENU_AVERAGE:
			if (!student)
				std::cout << "NO details were added" << endl;
			else
				std::cout << "The average grade of " << student->getCrsCount() << " courses is: " << student->getAvg() << endl << endl;
			break;
		case MENU_PRINT_COURSES:
			if (!student)
				cout << "NO details were added" << endl;
			else
			{
				cout << "Courses list for student " << student->getName() << endl;
				// TO DO: call the printCourses function (the function is implemented)
				printCourses(student->getCourses(), student->getCrsCount());
			}
			break;
		case MENU_EXIT:
			toExit = true;
			std::cout << "Goodbye!" << endl;
			break;
		default:
			std::cout << "No Such Option!" << endl << endl;
			break;
		}
	}

	if (student)
	{
		delete student;
	}
	system("pause");
	return 0;
}

